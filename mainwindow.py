import signal
import sys
import time

import qtmodern.styles as qtModernStyle
from PySide2.QtCore import (QCoreApplication, QSettings, QSize, Qt, QThread,
                            Slot, qDebug)
from PySide2.QtGui import QColor, QFont, QIcon, QPalette, QPixmap
from PySide2.QtWidgets import (QAction, QApplication, QBoxLayout, QHBoxLayout,
                               QLabel, QMainWindow, QMenu, QMessageBox,
                               QPushButton, QSizePolicy, QSpacerItem,
                               QStackedWidget, QStatusBar, QSystemTrayIcon,
                               QToolButton, QVBoxLayout, QWidget)

import fonts
import icons
import img
from code_review import CodeReview
from config import Config
from config_file import ConfigFile
from pr_puller import PRPuller
from pull_request import PullRequestData

signal.signal(signal.SIGINT, signal.SIG_DFL)


class MainWindow(QMainWindow):

    def __init__(self, parent=None):
        # Setup
        super(MainWindow, self).__init__(parent)
        self.windowTitle = "tfs-notifier"
        self.size = QSize(280, 400)
        self.pr_opened = 0
        self.version = "v0.0.1"
        self.current_view = ''

        self.setWindowTitle(self.windowTitle)
        self.setWindowIconText(self.windowTitle)
        self.setWindowIcon(QPixmap('assets/icons/logo.ico'))
        self.setDockNestingEnabled(True)
        self.setMinimumHeight(self.size.height())
        self.setMinimumWidth(self.size.width())
        self.setMaximumWidth(self.size.width()*2)
        self.setWindowFlag(Qt.CustomizeWindowHint, True)
        self.setWindowFlag(Qt.WindowMinimizeButtonHint, True)
        self.setWindowFlag(Qt.WindowMaximizeButtonHint, False)
        self.setWindowFlag(Qt.WindowCloseButtonHint, False)

        self.icon_status_message = {"OK": QPixmap('assets/icons/ok.png'),
                                    "Alert": QPixmap('assets/icons/alert.png'),
                                    "Problem": QPixmap('assets/icons/problem.png')}

        # App icon
        self.app_icon = QIcon(self.windowTitle)
        self.app_icon.addPixmap(QPixmap('assets/icons/logo.png'),
                                QIcon.Mode.Normal, QIcon.State.On)
        self.setWindowIcon(self.app_icon)

        # Tray Icon
        self.status_icon = {
            "Default": QPixmap('assets/img/logo.png'),
            "Warn": QPixmap('assets/img/logo-warn.png'),
            "OK": QPixmap('assets/img/logo-ok.png')
        }

        self.tray_icon = QSystemTrayIcon()
        self.loadSysTray()

        # Menu bar
        self.menu = QWidget()
        self.menu_layout = QHBoxLayout()
        self.loadMenu()

        # Status Bar
        self.statusBar = QStatusBar()
        self.layout_statusBar = QBoxLayout(QBoxLayout.LeftToRight, self)
        self.loadStatusBar()

        # Window widgets
        self.config = Config()
        self.code_review = CodeReview(parent=self)
        self.code_review.updated.connect(self.updatePrCount)

        self.stack = QStackedWidget(self)
        self.stack.addWidget(self.code_review)
        self.stack.addWidget(self.config)

        self.setCentralWidget(self.stack)

        # PR Puller
        self.pr_puller = PRPuller()
        self.pr_puller.refreshed.connect(self.updateStatusMessage)
        self.pr_puller.updated.connect(self.code_review.handlePullRequest)
        self.pr_puller.start()

        config = ConfigFile.getInstance()

        try:
            self.restoreGeometry(config.geometry)
            self.restoreState(config.window_state)
        except:
            pass

    def changeView(self, view: str = "Main"):
        if view == "Main":
            if self.current_view == "Config":
                self.config.Save()
                ConfigFile.getInstance().Load()
            self.stack.setCurrentIndex(0)
        elif view == "Config":
            self.stack.setCurrentIndex(1)

        self.current_view = view

    def showHome(self):
        self.changeView("Main")

    def showConfig(self):
        self.changeView("Config")

    def updatePrCount(self, pr_opened: int, pr_picked: int, id: int, reviewers: [str], status: str):
        # if self.pr_opened == pr_opened:
        #     return

        self.pr_opened == pr_opened
        text = ""

        if pr_opened != 0:
            text + " ("+str(pr_opened)+")"

        self.updateTrayIcon(pr_opened, pr_picked, id, reviewers, status)
        self.setWindowIconText(self.windowTitle + text)

    def onTrayIconActivated(self, reason):
        if reason == QSystemTrayIcon.Trigger:
            self.toggleVisibility()

    def toggleVisibility(self):
        if self.isActiveWindow() and self.isVisible():
            self.setVisible(not self.isVisible())
        else:
            if not self.isVisible():
                self.setVisible(True)
            self.activateWindow()

    def toggleAlwaysOnTop(self):
        state = self.saveState()

        if self.windowFlags() & Qt.WindowStaysOnTopHint:
            self.setWindowFlag(Qt.WindowStaysOnTopHint, False)
        else:
            self.setWindowFlag(Qt.WindowStaysOnTopHint, True)

        self.show()
        self.restoreState(state)

    def updateTrayIcon(self, pr_opened: int = 0, pr_picked: int = 0, id: int = 0,  reviewers: [str] = [], status: str = ""):
        if pr_opened == 0:
            tray_icon = "Default"
        elif pr_picked < pr_opened:
            tray_icon = "Warn"
        else:
            tray_icon = "OK"

        self.tray_icon.setIcon(self.status_icon[tray_icon])

        if pr_opened and status == "New" and len(reviewers) == 0:
            plural = "s" if pr_opened > 1 else ""
            self.tray_icon.showMessage(self.windowTitle,
                                       f"{pr_opened - pr_picked} pull request{plural} pending review",
                                       QSystemTrayIcon.MessageIcon.Warning,
                                       5000)
        elif status == "Update":
            if self.config.field_username.text() in reviewers:
                self.tray_icon.showMessage(self.windowTitle,
                                           f"Pull Request updated",
                                           QSystemTrayIcon.MessageIcon.Information,
                                           5000)

        qDebug(f"Changing tray icon to: {tray_icon}")

    def onClose(self):
        self.hide()

    def updateStatusMessage(self, status, message):
        self.status_message.setText(message)

        if status in self.icon_status_message.keys():
            icon = self.icon_status_message[status]
        else:
            icon = None

        self.status_message_icon.setPixmap(icon)

    def loadSysTray(self):
        self.updateTrayIcon()
        self.tray_icon.activated.connect(self.onTrayIconActivated)

        # Tray Menu
        self.tray_menu = QMenu()

        self.tray_menu.addAction(QAction(
            self.tr("Hide/Show"), self, triggered=self.toggleVisibility))

        if __debug__:
            self.tray_menu.addAction(QAction(self.tr("Insert"), self,
                                             triggered=self.testAlert))
            self.tray_menu.addAction(QAction(self.tr("Update"), self,
                                             triggered=self.testAlert2))
            self.tray_menu.addAction(QAction(self.tr("Delete"), self,
                                             triggered=self.testAlert3))

        self.tray_menu.addAction(
            QAction(self.tr("Exit"), self, triggered=self.close))

        self.tray_icon.setContextMenu(self.tray_menu)
        self.tray_icon.show()

    def loadMenu(self):
        self.button_pin = QPushButton()
        self.button_pin.setCheckable(True)
        self.button_pin.setIcon(QPixmap('assets/icons/pin.png'))
        self.button_pin.setIconSize(QSize(30, 30))
        self.button_pin.setToolTip("Always on Top")
        self.button_pin.clicked.connect(self.toggleAlwaysOnTop)

        self.button_home = QToolButton()
        self.button_home.setIcon(QPixmap('assets/icons/home.png'))
        self.button_home.setIconSize(QSize(30, 30))
        self.button_home.setToolTip("Home")
        self.button_home.clicked.connect(self.showHome)

        self.button_config = QToolButton()
        self.button_config.setFont(QFont('Times', 15))
        self.button_config.setIcon(QPixmap('assets/icons/settings.png'))
        self.button_config.setIconSize(QSize(30, 30))
        self.button_config.setToolTip("Config")
        self.button_config.clicked.connect(self.showConfig)

        self.menu_layout.addWidget(self.button_pin)
        self.menu_layout.addSpacing(160)
        self.menu_layout.addStretch(1)
        self.menu_layout.addWidget(self.button_home)
        self.menu_layout.addWidget(self.button_config)

        self.menu.setLayout(self.menu_layout)

        self.setMenuWidget(self.menu)

    def loadStatusBar(self):
        self.status_message = QLabel()
        self.status_message.setScaledContents(True)

        self.status_message_icon = QLabel()
        self.status_message_icon.setScaledContents(True)
        self.status_message_icon.setAlignment(Qt.AlignLeft)
        self.status_message_icon.setMaximumHeight(20)
        self.status_message_icon.setMaximumWidth(20)

        self.statusBar.addWidget(self.status_message_icon)
        self.statusBar.addWidget(self.status_message, 1)
        self.statusBar.addWidget(QLabel(self.version))

        self.setStatusBar(self.statusBar)

    def keyPressEvent(self, e):
        if e.key() == Qt.Key_Escape:
            self.close()
        if __debug__:
            if e.key() == Qt.Key_I:
                self.testAlert()
            elif e.key() == Qt.Key_U:
                self.testAlert2()
            elif e.key() == Qt.Key_D:
                self.testAlert3()

    def closeEvent(self, event):
        self.hide()
        self.pr_puller.working = False
        while self.pr_puller.isRunning():
            time.sleep(1)
        self.config.ini.Save("geometry", self.saveGeometry())
        self.config.ini.Save("window_state", self.saveState())
        self.config.Save()

        # close window
        event.accept()

    def testAlert(self):
        pr = PullRequestData(repo="SDP", id=500,
                             status="New", reviewers=[])
        self.code_review.handlePullRequest(pr)

    def testAlert2(self):
        pr = PullRequestData(repo="SDP", date="20/02/2002 22:22", id=500,
                             status="Update", reviewers=['Name'])
        self.code_review.handlePullRequest(pr)

    def testAlert3(self):
        pr = PullRequestData(id=500, status="Delete")
        self.code_review.handlePullRequest(pr)


def main():
    app = QApplication(sys.argv)
    qtModernStyle.light(app)
    window = MainWindow()
    window.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
