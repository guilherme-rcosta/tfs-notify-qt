#!/bin/bash

rcs=("img" "icons")
source ./venv/bin/activate

for rc in "${rcs[@]}"; do
    pyside2-rcc "${rc}.rc" -o "${rc}.py"
done
