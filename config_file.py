from PySide2.QtCore import (QCoreApplication, QSettings, QStringListModel, Qt,
                            QUrl, qDebug)


class ConfigFile(object):
    __instance = None
    user: str = ""
    dark_theme: bool = True
    auto_launch: bool = False
    url = "https://dev.azure.com"
    organization = ""
    projects: [str] = []
    token: str = "7qxdpy26pxppla6jld4eovweykipcoys2fg3duzojiu3bvey3cla"
    refresh_interval: str = 5
    geometry = None
    state = None
    labels = ["user", "dark_theme", "auto_launch", "url",
              "organization", "projects", "token",  "refresh_interval",
              "geometry", "window_state"]

    @staticmethod
    def getInstance():
        if ConfigFile.__instance == None:
            ConfigFile()
        return ConfigFile.__instance

    def __init__(self):
        if ConfigFile.__instance != None:
            raise Exception("This class is a Singleton!")
        else:
            ConfigFile.__instance = self

        QCoreApplication.setOrganizationName("tfs-notifier")
        QCoreApplication.setApplicationName("tfs.notifier.qt")
        QSettings.setDefaultFormat(QSettings.IniFormat)

        self.ini = QSettings(QSettings.UserScope)
        self.Load()

    def Load(self):
        self.user = self.ini.value("user", "")
        self.dark_theme = self.ini.value("dark_theme", False, bool)
        self.auto_launch = self.ini.value("auto_launch", False, bool)
        self.url = self.ini.value("url", "https://dev.azure.com")
        self.organization = self.ini.value("organization", "")
        self.projects = self.ini.value("projects", [], str)
        self.token = self.ini.value("token", "")
        self.refresh_interval = self.ini.value("refresh_interval", 5, int)
        self.geometry = self.ini.value("geometry", 5, int)
        self.window_state = self.ini.value("window_state", 5, int)

    def Save(self, label, value):
        if label in self.labels:
            self.ini.setValue(label, value)
        else:
            qDebug(f"ConfigFile >>> Label {label} not found!!!")
