$rcs = @('img', 'icons')

venv/Scripts/activate.ps1

foreach ( $rc in $rcs )
{
    pyside2-rcc "${rc}.rc" -o "${rc}.py"
}