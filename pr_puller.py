import datetime
import json
import time

import requests
from PySide2.QtCore import QObject, QThread, QTimer, Signal, Slot, qDebug
from requests.auth import HTTPBasicAuth
from requests.exceptions import HTTPError

from config_file import ConfigFile
from pull_request import PullRequestData


class PRPuller(QThread):
    updated = Signal(object)
    refreshed = Signal(str, str)
    prIds = {}

    def __init__(self, parent=None):
        super(PRPuller, self).__init__(parent)
        self.config = ConfigFile.getInstance()
        self.timer = QTimer()
        self.timer.timeout.connect(self.getPullRequests)
        self.working = True
        self.interval = 0
        self.count = 0
        self.prIds = {project: []
                      for project in self.config.projects.split(',')}

    def getPullRequests(self):
        for project in self.config.projects.split(','):
            url = '/'.join([self.config.url, self.config.organization,
                            project, "_apis/git/pullrequests"])
            print(url)

            try:
                response = requests.get(
                    url, auth=HTTPBasicAuth(None, self.config.token))
                # If the response was successful, no Exception will be raised
                response.raise_for_status()
            except HTTPError as http_err:
                print(f'HTTP error occurred: {http_err}')
                self.refreshed.emit("Alert", http_err.response.reason)
                return
            except Exception as err:
                print(f'Unexpected error occurred: {err}')
                self.refreshed.emit("Problem", self.tr("Connection Error"))
                return
            else:
                self.refreshed.emit("OK", self.tr(
                    f"Refreshed: {datetime.datetime.now()}"))
                qDebug(
                    f'Connection Success! {datetime.datetime.now().strftime("%X")}')

            data = json.loads(response.text)
            currentIds = []

            try:
                for item in data["value"]:
                    datestr_1 = item["creationDate"].replace("Z", "+00:00")
                    datestr = datestr_1[:19] + datestr_1[-6:]

                    date = datetime.datetime.fromisoformat(
                        datestr).strftime("%x %X")
                    id = item["pullRequestId"]
                    reviewers = [reviewer["displayName"]
                                 for reviewer in item["reviewers"]]
                    repo = item["repository"]["name"]

                    currentIds.append(id)
                    if id not in self.prIds[project]:
                        self.updated.emit(PullRequestData(project=project,
                                                          repo=repo, date=date, id=id, status="New", reviewers=reviewers))
                        self.prIds[project] += [id]

                for deletedId in set(self.prIds[project]) - set(currentIds):
                    self.updated.emit(PullRequestData(
                        id=deletedId, status="Deleted"))
                    self.prIds.remove(deletedId)
            except KeyError as e:
                qDebug(f"{e}")
            except ValueError as e:
                qDebug(f"{e}")

    def run(self):
        while self.working:
            self.getPullRequests()
            time.sleep(self.config.refresh_interval)


def main():
    prPuller = PRPuller()
    prPuller.updated.connect(printPR)
    prPuller.getPullRequests()


def printPR(pr: PullRequestData):
    print(f"Repo      : {pr.repo}")
    print(f"id        : {pr.id}")
    print(f"status    : {pr.status}")
    print(f"date      : {pr.date}")
    print(f"reviewers : {pr.reviewers}")


if __name__ == "__main__":
    main()
