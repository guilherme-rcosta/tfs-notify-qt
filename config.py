import qtmodern
from PySide2.QtCore import QSettings, QSize, QStringListModel, Qt, QUrl
from PySide2.QtGui import QDesktopServices, QPixmap
from PySide2.QtWidgets import (QApplication, QBoxLayout, QCheckBox, QComboBox,
                               QCompleter, QFormLayout, QLabel, QLayout,
                               QLineEdit, QPushButton, QSpinBox, QVBoxLayout,
                               QWidget)

from config_file import ConfigFile


class Config(QWidget):
    def __init__(self, parent=None):
        super(Config, self).__init__(parent)
        self.config_layout = QBoxLayout(QBoxLayout.TopToBottom, self)
        self.ini = ConfigFile.getInstance()

        self.username = QWidget()

        # Auto Launch
        self.auto_launch = QWidget()
        self.auto_init_layout = QBoxLayout(QBoxLayout.LeftToRight, self)
        self.auto_launch.setLayout(self.auto_init_layout)
        self.label_auto_launch = QLabel("Auto Launch")
        self.switch_auto_launch = QCheckBox()
        self.switch_auto_launch.toggled.connect(self.AutoLaunch)
        self.auto_init_layout.addWidget(self.switch_auto_launch)
        self.auto_init_layout.addWidget(self.label_auto_launch)
        self.auto_init_layout.addSpacing(1)
        self.auto_init_layout.addStretch(1)

        # Dark Theme
        self.theme = QWidget()
        self.theme_layout = QBoxLayout(QBoxLayout.LeftToRight, self)
        self.theme.setLayout(self.theme_layout)
        self.button_theme = QCheckBox()
        self.button_theme.toggled.connect(self.changeTheme)
        self.button_theme.setCheckable(True)
        self.button_theme.setChecked(self.ini.dark_theme)
        self.changeTheme()

        self.theme_layout.addWidget(self.button_theme)
        self.theme_layout.addWidget(QLabel(self.tr("Dark Theme")))
        self.theme_layout.addSpacing(1)
        self.theme_layout.addStretch(1)

        # Username
        self.label_username = QLabel(self.tr("User Name"))
        self.field_username = QLineEdit()
        self.field_username.setPlaceholderText(
            self.tr("Azure Devops Username"))
        self.names_completer = QCompleter()
        self.field_username.setCompleter(self.names_completer)
        self.names_completer.setCaseSensitivity(Qt.CaseInsensitive)
        self.names_completer.setCompletionMode(QCompleter.InlineCompletion)
        self.names_suggestions = [""]
        self.updateSuggestions()

        # URL
        self.label_tfs = QLabel(self.tr("Azure DevOps Url"))
        self.field_tfs = QLineEdit(self.ini.url)
        self.field_tfs.setPlaceholderText(self.tr("Azure DevOps base Url"))

        # Organization
        self.label_organization = QLabel(self.tr("Organization"))
        self.field_organization = QLineEdit(self.ini.organization)

        # Projects
        self.label_project = QLabel(self.tr("Projects"))
        self.field_project = QLineEdit(self.ini.projects)

        # App Token
        self.label_token = QLabel("Token")
        self.field_token = QLineEdit(self.ini.token)
        self.field_token.setPlaceholderText(self.tr("Personal Access Token"))

        # Refresh Interval
        self.label_refresh = QLabel(self.tr("Refresh Interval"))
        self.field_refresh = QSpinBox()
        self.field_refresh.setMinimum(5)
        self.field_refresh.setValue(self.ini.refresh_interval)
        self.field_refresh.setSuffix("s")

        self.config_layout.addWidget(self.auto_launch)
        self.config_layout.addWidget(self.theme)
        self.config_layout.addWidget(self.label_username)
        self.config_layout.addWidget(self.field_username)
        self.config_layout.addWidget(self.label_tfs)
        self.config_layout.addWidget(self.field_tfs)
        self.config_layout.addWidget(self.label_organization)
        self.config_layout.addWidget(self.field_organization)
        self.config_layout.addWidget(self.label_project)
        self.config_layout.addWidget(self.field_project)
        self.config_layout.addWidget(self.label_token)
        self.config_layout.addWidget(self.field_token)
        self.config_layout.addWidget(self.label_refresh)
        self.config_layout.addWidget(self.field_refresh)

        self.setLayout(self.config_layout)

    def setAutoLaunch(self, enable: bool):
        pass

    def updateSuggestions(self):
        model = QStringListModel()
        model.setStringList(self.names_suggestions)
        self.names_completer.setModel(model)

    def changeTheme(self):
        if self.button_theme.isChecked():
            qtmodern.styles.dark(QApplication.instance())
        else:
            qtmodern.styles.light(QApplication.instance())

    def Save(self):
        self.ini.Save("organization", self.field_organization.text())
        self.ini.Save("refresh_interval", self.field_refresh.value())
        self.ini.Save("url", self.field_tfs.text())
        self.ini.Save("token", self.field_token.text())
        self.ini.Save("user", self.field_username.text())
        self.ini.Save("dark_theme", self.button_theme.isChecked())
        self.ini.Save("projects", self.field_project.text())
        self.ini.Save("auto_launch", self.switch_auto_launch.isChecked())

    def AutoLaunch(self):
        if self.switch_auto_launch.isChecked():
            qtmodern.styles.dark(QApplication.instance())
        else:
            qtmodern.styles.light(QApplication.instance())
