import copy
import sys

from PySide2.QtCore import QSize, Qt, QUrl, Signal
from PySide2.QtGui import QDesktopServices
from PySide2.QtWidgets import (QAction, QBoxLayout, QFrame, QGroupBox, QLabel,
                               QPushButton, QVBoxLayout, QWidget)

from config_file import ConfigFile


class PullRequestData():
    def __init__(self, repo: str = "", project: str = "", date: str = "DD/MM/YY HH:mm:ss", id: int = 0, status: str = "New",  reviewers: [str] = []):
        self.repo = repo
        self.id = id
        self.status = status
        self.date = date
        self.reviewers = reviewers
        self.project = project


class PullRequestCard(QPushButton):
    def __init__(self, prData: PullRequestData):
        super(PullRequestCard, self).__init__()
        self.prData = prData
        self.config = ConfigFile.getInstance()
        self.styleSheet_danger = "color: #721c24; background-color: #f8d7da; border-color: #f5c6cb;"
        self.styleSheet_info = "color: #0c5460; background-color: #d1ecf1; border-color: #bee5eb;"

        self.setAutoFillBackground(True)

        self.clicked.connect(self.openUrl)

        self.layout = QBoxLayout(QBoxLayout.TopToBottom)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.labelId = QLabel()
        self.labelDate = QLabel()
        self.labelRepo = QLabel()
        self.labelReviewers = QLabel()
        self.updateLabels()

        self.group_box = QWidget()
        self.layout_group_box = QBoxLayout(QBoxLayout.TopToBottom)
        self.layout_group_box.addWidget(self.labelRepo)
        self.layout_group_box.addWidget(self.labelId)
        self.layout_group_box.addWidget(self.labelDate)
        self.layout_group_box.addWidget(self.labelReviewers)
        self.group_box.setLayout(self.layout_group_box)
        self.layout.addWidget(self.group_box)

        self.setFixedHeight(120)

        self.setLayout(self.layout)
        self.show()

    def UpdateData(self, prData: PullRequestData):
        self.prData = copy.copy(prData)
        self.updateLabels()

    def updateLabels(self):
        self.labelRepo.setText(self.prData.repo)
        self.labelId.setText("ID: " + str(self.prData.id))
        self.labelDate.setText(self.prData.date)
        self.labelReviewers.setText(
            'Reviewers: \n'+','.join(self.prData.reviewers))

        if len(self.prData.reviewers):
            self.setStyleSheet(self.styleSheet_info)
        else:
            self.setStyleSheet(self.styleSheet_danger)

    def openUrl(self):
        url = "/".join([self.config.url, self.config.organization, self.prData.project,
                        "_git", self.prData.repo, 'pullrequest', str(self.prData.id)])
        QDesktopServices.openUrl(QUrl(url, QUrl.TolerantMode))
