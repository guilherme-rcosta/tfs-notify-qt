import sys

from PySide2.QtCore import QSize, Qt, QThread, Signal, qDebug
from PySide2.QtGui import QIcon, QPixmap
from PySide2.QtWidgets import (QAction, QApplication, QFormLayout, QLabel,
                               QMenu, QScrollArea, QSystemTrayIcon,
                               QVBoxLayout, QWidget)

from pull_request import PullRequestCard, PullRequestData


class CodeReview(QScrollArea):
    updated = Signal(int, int, int, list, str)

    def __init__(self, parent=None):
        super(CodeReview, self).__init__(parent)
        self.opened = 0
        self.picked = 0
        self.layout = QFormLayout()
        self.layout.setContentsMargins(1, 1, 1, 1)
        self.widget = QWidget()
        self.widget.setLayout(self.layout)
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setWidgetResizable(True)
        self.setWidget(self.widget)

    def addPullRequest(self, pr: PullRequestData):
        prCard = PullRequestCard(pr)
        self.layout.addRow(prCard)
        self.opened += 1
        if len(pr.reviewers):
            self.picked += 1
        else:
            QApplication.instance().beep()

    def updatePullRequest(self, pr: PullRequestData):
        items = (self.layout.itemAt(i) for i in range(self.layout.count()))

        for item in items:
            try:
                prData = item.wid.prData
                if prData.id == pr.id:
                    if len(prData.reviewers) == 0 and len(pr.reviewers) > 0:
                        self.picked += 1
                    elif len(prData.reviewers) > 0 and len(pr.reviewers) == 0:
                        self.picked -= 1
                    item.wid.UpdateData(pr)
                    return
            except:
                qDebug(f"<code_review> {sys.exc_info()[0]}")
                continue

    def removePullRequest(self, id):
        for i in range(self.layout.count()):
            item = self.layout.itemAt(i)
            if item.wid.prData.id == id:
                self.opened -= 1

                if len(item.wid.prData.reviewers):
                    self.picked -= 1

                item.wid.hide()
                qDebug(f"<code_review> Deleting PullRequest {id}!!!")
                self.layout.removeItem(item)
                del item
                return
        qDebug(f"<code_review> PullRequest {id} not found!!!")

    def handlePullRequest(self, pr: PullRequestData):
        qDebug(f"{pr.status} PullRequest: {pr.id} {pr.reviewers}")

        if pr.status == "New":
            self.addPullRequest(pr)
        elif pr.status == "Update":
            self.updatePullRequest(pr)
        else:
            self.removePullRequest(pr.id)

        qDebug(
            f"<code_review> CRs opened({self.opened}) / picked({self.picked})")

        self.updated.emit(self.opened, self.picked,
                          pr.id, pr.reviewers, pr.status)
